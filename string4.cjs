const string4 = (obj) => {
    let fullName = ''
    const names = Object.values(obj)

    for (let index = 0; index < names.length; index++) {
        if (names[index].length > 0) {
            let firstLetter = names[index].trim().slice(0, 1).toUpperCase()

            names[index] = firstLetter + names[index].trim().slice(1).toLowerCase()
            fullName += ' ' + names[index]
        }
    }

    return fullName.trim()
}

module.exports = string4