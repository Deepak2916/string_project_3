const string2 = (ip) => {
    let ipArr = ip.split('.')

    let result = []
    if (ipArr.length !== 4) {
        return []
    }
    let flag = ipArr.every(str => {
        let num = parseInt(str)
        result.push(num)
        return !isNaN(num) && str.length <= 3 && num <= 255 && num >= 0 && num.toString() == str
    })
    if (flag) {
        return result
    }
    return []


}

module.exports = string2