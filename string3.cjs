const string3 = (date) => {

   let dateArr = date.split('/')
   if (dateArr[0].length > 2 || dateArr[1].length > 2 || parseInt(dateArr[2]) < 1 ||parseInt(dateArr[1])<1 || parseInt(dateArr[0])<1) {
      return 'invalid'
   } else if (dateArr[2].length > 2 && parseInt(dateArr[0]) !== dateArr) {
      return 'invalid'
   }
   for (let index = 0; index < dateArr.length; index++) {
      dateArr[index] = parseInt(dateArr[index])
   }

   const checkLeapYear = (year) => {
      // console.log(year%4==0  && year%100!==0)
      if (year % 4 == 0 && year % 100 !== 0) {
         return true
      }
      else {
         return false
      }
   }

   let months = [
      ['January',31], 
      ['February',28],
       ['March',31], 
       ['April',30], 
       ['May',31], 
       ['June',30], 
       ['July',31], 
       ['August',31], 
       ['September',30], 
       ['October',31], 
       ['November',30], 
       ['December',31]]


   if (dateArr.length === 3 && dateArr[0] < 32 && dateArr[1] < 13 && !dateArr.includes(NaN)) {
      if (dateArr[1] == 2) {
         // console.log(dateArr)
         if (checkLeapYear(dateArr[2]) && dateArr[0] < 30) {
            return months[dateArr[1] - 1][0]
         }
         else if (!checkLeapYear(dateArr[2]) && dateArr[0] < 29) {
            // console.log(!checkLeapYear(dateArr[2]))
            return months[dateArr[1] - 1][0]
         }

         else {
            return 'invalid'
         }
      }
      else {
         // console.log(dateArr[1],months[dateArr[1]])
         if(dateArr[0]<=months[dateArr[1] - 1][1]){
         return months[dateArr[1] - 1][0]
         }
         return 'invalid'
      }
   }
   else {
      return 'invalid'
   }
}


module.exports = string3