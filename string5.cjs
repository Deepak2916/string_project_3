const string5 = (array) => {

    if (array.length>0) {
        // console.log(array,'array')
        let sentence = '';
        for (let index = 0; index < array.length; index++) {
            sentence += ' ' + array[index].trim()

        }

        sentence = sentence.trim()
        return sentence + '.'
    }
    return []
}

module.exports = string5